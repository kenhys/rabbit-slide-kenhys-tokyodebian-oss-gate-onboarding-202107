# OSS Gate\\nオンボーディング

subtitle
:  Debianプロジェクトを題材にした取り組みの紹介

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  2021年7月 東京エリア・関西合同Debian勉強会

allotted-time
:  15m

theme
:  .

# スライドは公開済みです

* OSS Gateオンボーディング - Debianプロジェクトを題材にした取り組みの紹介
  * <https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-oss-gate-onboarding-20210717/>

# 本日の内容

* OSS Gateオンボーディング
  * そもそもOSS Gateとは何?
  * OSS Gateオンボーディングとは何?
  * Debianプロジェクト向けに実施するなら?
  * 参加する人のメリットは?
  * どんな感じで進めるのか?

# OSS Gateとは何?

![https://oss-gate.github.io/](images/oss-gate.png)

* OSS開発に参加する人を継続的に増やす取り組み
  
# OSS Gateオンボーディング?

![https://oss-gate.github.io/on-boarding/](images/onboarding.png)

* 「継続的にOSSを開発する人」を増やすことを目指す取り組み **(新しくはじめました！)**

# オンボーディング?

* 一般的に企業で入社後の研修などを指す
  * 新しくはいってきた人が活躍できるようにもろもろ支援する
    * 例: 組織の考えを理解するのが大事なら、その機会を設ける
    * 例: 前提となる必要な知識・スキルがあれば、その習得を支援する

# OSS Gateオンボーディング?

* 新しくはいってきた人が対象OSSで力を発揮できるようにもろもろ支援する
  * 例: 対象OSSの開発体制やポリシーの理解が必要ならその機会を設ける
  * 例: 特定のツールを対象OSSで開発時に採用しているならその使い方を伝授する

# Debianプロジェクトの課題(個人の感想です)

* 人的リソースが足りていない問題
  * <https://salsa.debian.org/freexian-team/project-funding> のようにDebianにとって価値あるプロジェクトに資金援助する仕組みはある(まだ実施1件)
* 野生のDebian開発者は(なかなか)生えてこない
* だんだん参加者が高齢化していく危機感 👴👵
  * オンボーディングみたいな取り組みの時間がとりにくい

# OSS Gateオンボーディングの大まかな流れは?

* OSS開発参加者が増えて欲しい人(先輩)とOSS開発に参加したい人(新人)をマッチング
* 先輩が**業務時間を使って**新人と一緒に開発する
  * 休日などにまとまった時間が取りにくい問題をこれで解決
* 企業はオンボーディングを先輩の業務とすることでスポンサーする

# OSS Gateオンボーディング案 for Debian

* Debianプロジェクトに関わる人(新人)を増やしたい
  * 新規にパッケージングする人
  * 既存のパッケージをメンテナンスする人
  * パッケージの不具合を報告・修正する人
  * Debianプロジェクト関連のシステムをメンテナンスする人

# 具体的なOSS Gateオンボーディング案 for Debian

<https://oss-gate.github.io/on-boarding/proposals/2021-08/kenhys-maintain-debian-packages/>

* 新規パッケージングコース
  * ITPからRFS、スポンサーアップロードに取り組む
* 既存の不具合修正コース
  * bugs.d.oを活用して既知の問題に取り組む
* mentors.d.n改善コース
  * 登録されているissueの問題に取り組む

# 新人のメリット

* 新規パッケージングコース
  * ITPからRFS、スポンサーアップロードなどパッケージングに関する知識・経験が得られる
* 既存の不具合修正コース
  * bugs.d.oを活用した問題解決の知識・経験が得られる
* mentors.d.n改善コース
  * mentors.d.nを改善するのに必要な知識・経験が得られる

# 先輩のメリット

* Debianプロジェクトに参加する新人が増える
  * コミュニティの活性化につながる
* **業務として**Debianプロジェクトの開発に取り組める
  * プライベートな時間をあまり割けない状況を改善できる
* 新人がつまづきやすいポイントに気づける
  * 新しく人が参加しやすい環境を整えやすくなる

# どんな感じで進めるのか?

* 先輩は @kenhys が担当します
* 初回スポンサーはクリアコードが担当します
* 募集期間は7/31(土)  23:59:59 (JST)まで
* 支援期間は2021-08-10から2021-10-05を想定
* 募集人数: 1名

# 応募方法

* <https://oss-gate.github.io/on-boarding/proposals/2021-08/kenhys-maintain-debian-packages/#how-to-apply>
  * 応募先はこちら! <on-boarding@oss-gate.org>
    * 名前(GitHubのIDなどでも可)
    * 応募動機/応募コース
    * 現時点での自分のOSS開発に関する知識・経験
    * 活動予定時間(都合のよい時間帯)
    * どこでこの活動を知ったか

# さいごに

* OSS GateオンボーディングをDebianプロジェクト向けにはじめます
  * <https://oss-gate.github.io/on-boarding/>
* Debianパッケージのメンテナンスやそれを支えるシステムを題材に取り組みたい人を募集します
* (応募してくれる人を見つけないといけないので)告知に協力してくれる人はいませんか?
* 将来的にはスポンサーしたい企業(担当者)も増やしたい

# 補足:告知の協力方法

![https://twitter.com/kenhys/status/1415850305044901888 をリツイート!](images/tweet.png){:relative-height="70"}

* <https://oss-gate.github.io/on-boarding/> で募集している旨をツイート

